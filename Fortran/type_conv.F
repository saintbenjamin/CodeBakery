      program type_conv
        
        implicit none
        real x(2)
        double precision xx(2)
        integer i
        
        read(*,*) xx(1)
        read(*,*) xx(2)
        x = REAL(xx)
        write(*,'(e23.16,1x,e23.16)') (x(i),xx(i),i=1,2)

      stop
      end
