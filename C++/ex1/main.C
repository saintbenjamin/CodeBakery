#include <iostream>
#include "sub.h"
#include "class_template.h"  // for template specialization example
using namespace std;

int main () {

//==========================================================  
// template specialization
//==========================================================  
  int m = 7;
  double x = 12.67;
  mycontainer<int> myint (m);
  mycontainer<double> mydouble (x);

  cout << myint.increase() << endl;
  
  cout << mydouble.increase() << endl;
  sub1(x);
  cout << x << endl;
//==========================================================  
  
  
  return 0;
}
