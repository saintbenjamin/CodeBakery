#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_errno.h>
#include "func_chisq.h"

#define MAXPAR 12
#define MAXITER 10000
#define TOL_MM_GRAD 1.0e-02
#define TOL_MM_DELF 1.0e-11
#define TOL_MM_DELX 1.0e-08

int nlm_derivative(gsl_vector *params, struct FitData const fit);
int nlm_simplex(gsl_vector *params, struct FitData const fit);


int multiMinChiSq(int n1, int n2, int nt, int npar)
{
  struct FitData fit;
  fit.n1 = n1;
  fit.n2 = n2;
  fit.nt = nt;
  
  int npt = fit.n2 - fit.n1 + 1;
  double dtmp;
  char stmp[30];
  
  gsl_vector *corr = gsl_vector_alloc(fit.nt);
  gsl_vector *m = gsl_vector_alloc(npt);
  gsl_matrix *corr_err = gsl_matrix_alloc(fit.nt,fit.nt);
  gsl_matrix *ic = gsl_matrix_calloc(npt,npt);

  fit.avg = m;
  fit.invcov = ic;

  fit.nparam = npar;

#include "test_data.h"

  for (int i=0; i<npt; i++) {
    dtmp = gsl_vector_get(corr,fit.n1+i);
    gsl_vector_set(m,i,dtmp);
    dtmp = gsl_matrix_get(corr_err,fit.n1+i,fit.n1+i);
    gsl_matrix_set(ic,i,i,1./(dtmp*dtmp));
  }

  gsl_vector *c = gsl_vector_calloc(fit.nparam);
  gsl_vector *p = gsl_vector_calloc(fit.nparam);
  gsl_vector *iw = gsl_vector_calloc(fit.nparam);

  fit.p = p;
  fit.iw = iw;

  double ig[MAXPAR] = {0.1, 2.0, 0.5, 0.2, 
                       5.0, 0.5, 0.5, 0.2,
                       5.0, 0.5, 0.5, 0.2};

  // set initial guess 
  // and use it as a prior central value
  for ( int i=0; i<npar; i++ ) {
    scanf("%s",stmp);
    if ( strcmp(stmp,"-") == 0 ) { // default
      gsl_vector_set(c,i,ig[i]);
      gsl_vector_set(p,i,ig[i]);
    }
    else {
      sscanf(stmp,"%lf",&dtmp);
      gsl_vector_set(c,i,dtmp);
      gsl_vector_set(p,i,dtmp);
    }
  }
 
  // set prior width inverse
  for ( int i=0; i<npar; i++ ) {
    scanf("%s",stmp);
    if ( strcmp(stmp,"-") == 0 ) { // default
      gsl_vector_set(iw,i,0.0);
    }
    else {
      sscanf(stmp,"%lf",&dtmp);
      gsl_vector_set(iw,i,dtmp);
    }
  }

  printf("running nonlinear minimizer with derivative\n");
  nlm_derivative(c,fit);
  
//  nlm_simplex(c,&fit);
  
  gsl_vector_free(corr);
  gsl_vector_free(m);
  gsl_matrix_free(corr_err);
  gsl_matrix_free(ic);
  gsl_vector_free(c);
  gsl_vector_free(p);
  gsl_vector_free(iw);

  return 0;
}



int nlm_derivative(gsl_vector *c, struct FitData const fit)
{
  int npt = fit.n2 - fit.n1 + 1;
  int npar = fit.nparam;
  int dof = npt - npar;
  
  printf("(n1,n2) = (%d,%d)\n", fit.n1, fit.n2);
  printf("npar = %d, dof = %d\n",npar,dof);

  gsl_multimin_function_fdf chiSquared;
  chiSquared.n = npar;
  chiSquared.f = chiSq;
  chiSquared.df = dchiSq;
  chiSquared.fdf = chiSq_dchiSq;
  chiSquared.params = (void*) &fit;

  const gsl_multimin_fdfminimizer_type *T;
  //T = gsl_multimin_fdfminimizer_conjugate_fr;
  //T = gsl_multimin_fdfminimizer_steepest_descent;
  T = gsl_multimin_fdfminimizer_vector_bfgs2;

  gsl_multimin_fdfminimizer *s;
  s = gsl_multimin_fdfminimizer_alloc(T,npar);
  //gsl_multimin_fdfminimizer_set(s, &chiSquared, c, 0.01, 1e-4);
  gsl_multimin_fdfminimizer_set(s, &chiSquared, c, 0.1*gsl_blas_dnrm2(c), 0.1);
  
  double f_old, f_new, delta_f, delta_x, grad;
  size_t iter;
  int status;
 
  //gsl_vector *ig = gsl_vector_alloc(npar);
  //for ( int i=0; i<npar; i++ ) {
  //  gsl_vector_set(ig,i,gsl_vector_get(c,i));
  //}

  f_old = 0.0;
  iter = 0;
  do {
    iter++;
    status = gsl_multimin_fdfminimizer_iterate(s);
  
    f_new = s->f;
    delta_f = 2.0*fabs((f_old-f_new))/(f_old+f_new);
    delta_x = gsl_blas_dnrm2(s->dx)/gsl_blas_dnrm2(s->x);
    grad = gsl_blas_dnrm2(s->gradient);
    
    if (status == GSL_ENOPROG) 
    {
      printf("need gradient check --> ");
      ( gsl_multimin_test_size(grad,TOL_MM_GRAD) == GSL_SUCCESS )
       ? printf ("%s%g%s\n%s\n",
           "check passed (tol = ",TOL_MM_GRAD,")","Minimum found at:")
       : printf("line search failed to find a minimum (tol = %g)\n",
           TOL_MM_GRAD);
      break;
    }
    else if (status == GSL_SUCCESS)
    {
      if ( gsl_multimin_test_size(delta_f,TOL_MM_DELF) == GSL_SUCCESS &&
           gsl_multimin_test_size(delta_x,TOL_MM_DELX) == GSL_SUCCESS ) 
      {
        printf ("Minimum found at:\n");
        break;
      } 
      else {
        f_old = f_new;
      }
    }
    else {
      printf("error: check iterator status (= %2d)\n",status);
      exit(1);
    }
  }
  while ( iter < MAXITER);
     
  printf("iter = %d/%d\n"
         "chisq = %13.6le\n"
         "cdf = %13.6le\n"
         "rel_del_f = %13.6le (tol = %g)\n"
         "rel_del_c = %13.6le (tol = %g)\n"
         "grad = %13.6le\n",
         (int)iter, (int)MAXITER,
         f_new, 
         f_new/((double)dof),
         delta_f, ((double)TOL_MM_DELF),
         delta_x, ((double)TOL_MM_DELX),
         grad);
  printf("%8s %8s %8s %13s %13s\n",
      "c[]", "p[]", "iw[]", "rel_del_c[]", "grad[]");
  for ( int i=0; i<npar; i++ ) {
    printf("%8.5f ",gsl_vector_get(s->x,i));
    printf("%8.5f ",gsl_vector_get(fit.p,i));
    printf("%8.5f ",gsl_vector_get(fit.iw,i));
    printf("%13.6le ",gsl_vector_get(s->dx,i)/gsl_vector_get(s->x,i));
    printf("%13.6le",gsl_vector_get(s->gradient,i));
    printf("\n");
  }
  
  gsl_multimin_fdfminimizer_free(s);

  return 0;
}


#if 0
int nlm_simplex(struct *fit)
{
  gsl_multimin_function chiSquared;

  chiSquared.n = fit.nparam;
  chiSquared.f = chiSq;
  chiSquared.params = (void*) &fit;

  const gsl_multimin_fminimizer_type *T;
  gsl_multimin_fminimizer *s;
  gsl_vector *step = gsl_vector_alloc(fit.nparam);
  
  T = gsl_multimin_fminimizer_nmsimplex2rand;
  //T = gsl_multimin_fminimizer_nmsimplex2;
  s = gsl_multimin_fminimizer_alloc(T, fit.nparam);

  gsl_vector_set(step,0,0.1);
  gsl_vector_set(step,1,0.1);
  gsl_vector_set(step,2,0.1);
  gsl_vector_set(step,3,0.1);
  gsl_vector_set(step,4,0.1);
  gsl_vector_set(step,5,0.1);
  gsl_vector_set(step,6,0.1);
  gsl_vector_set(step,7,0.1);

  gsl_multimin_fminimizer_set(s, &chiSquared, c, step);
  
  do {
    iter++;
    status = gsl_multimin_fminimizer_iterate(s);

    if (status) {
      printf("test point A\n");
      printf ("%5d "
          "%.5f %.5f %.5f %.5f "
          "%.5f %.5f %.5f %.5f "
          "%13.6le\n", (int)iter,
              gsl_vector_get(s->x,0), 
              gsl_vector_get(s->x,1), 
              gsl_vector_get(s->x,2), 
              gsl_vector_get(s->x,3), 
              gsl_vector_get(s->x,4), 
              gsl_vector_get(s->x,5), 
              gsl_vector_get(s->x,6), 
              gsl_vector_get(s->x,7), 
              gsl_multimin_fminimizer_minimum(s));
      break;
    }

    printf ("%5d "
        "%.5f %.5f %.5f %.5f "
        "%.5f %.5f %.5f %.5f "
        "%13.6le\n", (int)iter,
            gsl_vector_get(s->x,0), 
            gsl_vector_get(s->x,1), 
            gsl_vector_get(s->x,2), 
            gsl_vector_get(s->x,3), 
            gsl_vector_get(s->x,4), 
            gsl_vector_get(s->x,5), 
            gsl_vector_get(s->x,6), 
            gsl_vector_get(s->x,7), 
            gsl_multimin_fminimizer_minimum(s));
  }
  while (iter < 1000);
  
  gsl_multimin_fminimizer_free(s);
  gsl_vector_free(c);
  gsl_vector_free(m);
  gsl_vector_free(corr);
  gsl_matrix_free(ic);
  gsl_matrix_free(corr_err);

  return 0;
}
#endif
