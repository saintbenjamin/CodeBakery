#include <stdio.h>
#include <stdlib.h>
#include "type.h"

#define N 10

int structure(void){

  int i;
  const int n = N;
  double *ptr;
  mycomplex *z;
  
  ptr = (double *) malloc(sizeof(double)*2*n);
  z = (mycomplex *) malloc(sizeof(mycomplex)*n);
  
  for(i=0; i<2*n; i++){
    *(ptr+i) = 823.12/(i+7);
  }
  
  for(i=0; i<n; i++){
   z = (void *)ptr;
  }


  for(i=0; i<2*n; i++){
    printf("%3.5lf\n",*(ptr+i));
  }
  
  for(i=0; i<n; i++){
    printf("%3.5lf, %3.5lf\n",(z+i)->re,(z+i)->im);
  }
  
  return 0;
}

#undef N
