#include "matrix.h"


//-------------------------------------------------------------------------
// GSL Matrix operation wrappers
//-------------------------------------------------------------------------
// NOTE: support only a square matrix
//-------------------------------------------------------------------------

// return trace of matrix A
double traceMat(gsl_matrix *matA, int n){

  double tr = 0.0;
  
  for( int i = 0; i < n; i++ )
    tr += gsl_matrix_get(matA,i,i);

  return tr;
}


// matrix multiplication A*B updates matrix B; matrix A dose not altered.
int multMat(gsl_matrix *matA, gsl_matrix *matB, int n){

  gsl_matrix *Tmp = gsl_matrix_calloc(n,n);
  double a;

  for( int i = 0; i < n; i++ ){
    for( int j = 0; j < n; j++ ){
      a = 0.0;
      for( int k = 0; k < n; k++ ){
        a += gsl_matrix_get(matA,i,k) * gsl_matrix_get(matB,k,j);
      }
      gsl_matrix_set(Tmp,i,j,a);
    }
  }
  gsl_matrix_memcpy(matB,Tmp);

  return 0;
}


// add a constant multiple of the identity matrix to the matrix A
int addMatConstMultId(gsl_matrix *matA, double c, int n){

  double a;

  for( int i = 0; i < n; i++ ){
    a = gsl_matrix_get(matA,i,i) + c;
    gsl_matrix_set(matA,i,i,a);
  }

  return 0;
}


// print matrix A
int printMat(gsl_matrix *matA, int n){
  
  for( int i = 0; i < n; i++ ){
    for( int j = 0; j < n; j++ ){
      printf("%13.6le,",gsl_matrix_get(matA,i,j));
    }
    printf("\n");
  }
  printf("\n");

  return 0;
}
