int array(void);
int multi_dim_array(void);
int pointer(void);
int structure(void);
int print(void);
int typeconv(void);
int exponent(void);
int associativity(void);
int checkWordsize(void);
int division(void);
int multipleAssign(void);
int matInv_FaddevLeverrier(void);
int multiMinChiSq(int n1, int n2, int nt, int npar);
