#include <stdio.h>
#define N 10

int multi_dim_array(void){

 double aone[N];
 double atwo[2][N];
 double *p;

 for(int i=0; i<N; i++){
   aone[i] = 0.25*(i+1);
 }
 
 for(int i=0; i<N; i++){
   atwo[0][i] = aone[i];
   atwo[1][i] = 100*aone[i];
   printf("aone[%d]=%lf, atwo[0][%d]=%lf, atwo[1][%d]=%lf\n",
       i,aone[i],i,atwo[0][i],i,atwo[1][i]);
 }

 printf("\n*atwo[0]=%lf\n",*atwo[0]);
 printf("*atwo[1]=%lf\n",*atwo[1]);

 printf("\nlet pointer p = atwo[0]\n");
 p = atwo[0];
 for(int i=0; i<N-1; i++){
   printf("*(p+%d)=%lf\n",i,*p);
   p++;
 }
 
 printf("\nlet pointer p = atwo[1]\n");
 p = atwo[1];
 for(int i=0; i<N-1; i++){
   printf("*(p+%d)=%lf\n",i,*p);
   p++;
 }


 return 0;
}

#undef N
