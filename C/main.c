#include <stdio.h>
#include <stdlib.h>
#include "examplelist.h"

int main(int argc, char *argv[]){
/*
  array();
  pointer();
  structure();
  print();
  typeconv();
  exponent();
  associativity();
  checkWordsize();
  division();
  multipleAssign();
  matInv_FaddevLeverrier();
  multi_dim_array();
*/
  multi_dim_array();

/*
  if( argc == 5 ){
    multiMinChiSq(atoi(argv[1]),atoi(argv[2]),atoi(argv[3]),atoi(argv[4]));
  }else{
    printf("need four arguments in successive order, n1, n2, nt, npar\n");
  }
*/
  return 0;
}
