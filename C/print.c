#include <stdio.h>

int print(void){
  
  float flt = 1.0;
  double dbl = 2.0;
  
  printf("format = le : float variable = %le\n", flt);
  printf("format = e : float variable = %e\n", flt);
  printf("format = le : double variable = %le\n", dbl);
  printf("format = e : double variable = %e\n", dbl);

  return 0;

}
